import os
import re
import sys
from glob import iglob
from pathlib import Path

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from tqdm import tqdm


pattern = re.compile('^from_(\d+)_to_(\d+)$')


def convert_label(label):
    match = pattern.match(label)
    return '{}-{}'.format(match.group(1), match.group(2))


def extract_label_from_path(path):
    label = Path(path).parent.name
    label = convert_label(label)
    return label


def build_name_to_label(file):
    name_to_label = dict()
    for path in map(str.strip, file):
        label = extract_label_from_path(path)
        name = Path(path).name
        name_to_label[name] = label
    return name_to_label


if __name__ == '__main__':
    name_to_label = build_name_to_label(sys.stdin)

    input_directory = sys.argv[1]
    output_directory = sys.argv[2]
    os.makedirs(output_directory, exist_ok=True)

    file_pattern = os.path.join(input_directory, '*.jpg')

    for image_path in tqdm(iglob(file_pattern), miniters=50, mininterval=1):
        name = Path(image_path).name
        if name not in name_to_label:
            continue
        label = name_to_label[name]
        with Image.open(image_path) as image:
            directory = os.path.join(output_directory, label)
            os.makedirs(directory, exist_ok=True)
            new_image_path = os.path.join(directory, name)
            image.save(new_image_path)