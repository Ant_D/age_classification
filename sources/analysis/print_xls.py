import re
import sys

import pandas as pd


pattern = re.compile(r'^from_(\d+)_to_(\d+)$')


def convert_label(label):
    match = pattern.match(label)
    return '{}-{}'.format(match.group(1), match.group(2))


for path in map(str.strip, sys.stdin):
    table = pd.read_excel(path)
    for col in table.columns:
        if col.startswith('from'):
            table[col] = table[col].map(lambda x: round(x, 2))
    table['Unnamed: 0'] = table['Unnamed: 0'].map(convert_label)
    table = table.rename(lambda c: convert_label(c) if c.startswith('from') else c, axis='columns')

    print(table)