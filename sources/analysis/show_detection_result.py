import os
import sys
from pathlib import Path

from PIL import Image
from PIL import ImageDraw


output_directory = sys.argv[1]
os.makedirs(output_directory, exist_ok=True)

for image_path in map(str.strip, sys.stdin):
    with Image.open(image_path) as image:
        draw = ImageDraw.Draw(image)
        for line in map(str.strip, sys.stdin):
            if not line:
                break
            x1, y1, x2, y2, label, confidence = map(float, line.split(' '))
            x1 = int(x1 * image.width)
            y1 = int(y1 * image.height)
            x2 = int(x2 * image.width)
            y2 = int(y2 * image.height)
            draw.rectangle([x1, y1, x2, y2], width=4)
        image_name = Path(image_path).name
        result_image_path = os.path.join(output_directory, image_name)
        image.save(result_image_path)
