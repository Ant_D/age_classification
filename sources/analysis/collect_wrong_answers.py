import re
import sys
from pathlib import Path

import numpy as np

pattern = re.compile('^from_(\d+)_to_(\d+)$')


def convert_label(label):
    match = pattern.match(label)
    return '{}-{}'.format(match.group(1), match.group(2))


def parse_labels_and_probs():
    labels = []
    probs = []

    for result in map(str.strip, sys.stdin):
        if not result:
            break
        label, prob = result.split(' ')
        prob = float(prob)
        labels.append(label)
        probs.append(prob)

    return labels, probs


def extract_label_from_path(path):
    label = Path(path).parent.name
    label = convert_label(label)
    return label


if __name__ == '__main__':

    for image_path in map(str.strip, sys.stdin):
        labels, probs = parse_labels_and_probs()
            
        true_label = extract_label_from_path(image_path)
        true_label_index = labels.index(true_label)

        predicted_label_index = np.argmax(probs)
        predicted_label = labels[predicted_label_index]

        if abs(true_label_index - predicted_label_index) > 1:
            print(image_path)
