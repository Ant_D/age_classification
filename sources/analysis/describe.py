import sys

import numpy as np
import pandas as pd


for path in map(str.strip, sys.stdin):
    df = pd.read_excel(path)
    
    labels = df.columns[1:].values
    matrix = np.array([
        row[1:] for row in df.values
    ])

    diagonal = []
    for i in range(len(labels)):
        diagonal.append(matrix[i][i])

    neighbors = []
    for i in range(len(labels)):
        flags = np.zeros(len(labels), dtype=bool)
        indexes = [i]
        if i > 0:
            indexes.append(i - 1)
        if i < len(labels) - 1:
            indexes.append(i + 1)
        flags[indexes] = True
        neighbors.append(flags)

    neighbor_sum = []
    for i in range(len(labels)):
        value = np.sum(matrix[i][neighbors[i]])
        neighbor_sum.append(value)

    print(path)
    # print(df)
    for i in range(len(labels)):
        print(labels[i], diagonal[i], neighbor_sum[i])
    print()
