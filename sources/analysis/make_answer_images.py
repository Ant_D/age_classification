import os
import re
import shutil
import sys
from pathlib import Path

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from collect_wrong_answers import extract_label_from_path


IMAGE_SIZE = 224
IMAGE_SHAPE = (IMAGE_SIZE, IMAGE_SIZE)


def create_image_with_answer(image_path, answer):
    with Image.open(image_path) as image:
        image = image.resize(size=IMAGE_SHAPE)

        text_image = Image.new('RGB', IMAGE_SHAPE)
        draw = ImageDraw.Draw(text_image)
        font = ImageFont.truetype('arial.ttf', 12)
        draw.text((0, 0), answer, font=font)

        image_with_answer = Image.new('RGB', (IMAGE_SIZE, 2 * IMAGE_SIZE))
        image_with_answer.paste(image)
        image_with_answer.paste(text_image, box=(0, IMAGE_SIZE))

        return image_with_answer


def read_answer(file):
    answer = ''
    for result in map(str.strip, sys.stdin):
        if not result:
            break
        answer += result + '\n'
    return answer


if __name__ == '__main__':
    output_directory = sys.argv[1]
    os.makedirs(output_directory, exist_ok=True)
    
    for image_path in map(str.strip, sys.stdin):
        answer = read_answer(sys.stdin)
        answer = 'true label: ' + extract_label_from_path(image_path) + '\n' + answer
        image = create_image_with_answer(image_path, answer)
        image_name = Path(image_path).name
        new_image_path = os.path.join(output_directory, image_name)
        image.save(new_image_path)