import re
import sys
from pathlib import Path

CATEGORIES = ['00-02', '03-07', '08-12', '13-18', '19-26', '27-35', '36-48', '49-60', '61-75', '76-90']
MAX_DIFFERENCE = 1

def parse_category(path):
    directory_name = Path(path).parent.name
    pattern = r'from_(\d{2})_to_(\d{2})'
    regex = re.compile(pattern)
    match = regex.match(directory_name)
    begin = match.group(1)
    end = match.group(2)
    return f'{begin}-{end}'


test_count = 0
correct_count = 0
answer_file_path = sys.argv[1]
with open(answer_file_path, 'r') as answer_file:
    answers = map(str.strip, answer_file)
    paths = map(str.strip, sys.stdin)
    for path, answer in zip(paths, answers):
        answer_id = CATEGORIES.index(answer)
        true_answer = parse_category(path)
        true_answer_id = CATEGORIES.index(true_answer)
        test_count += 1
        if abs(answer_id - true_answer_id) <= MAX_DIFFERENCE:
            correct_count += 1
    print(correct_count / test_count)
