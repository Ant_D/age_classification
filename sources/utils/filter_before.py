import sys

marker = sys.argv[1]

allowed = False
lines = map(str.strip, sys.stdin)
for line in lines:
	if line == marker:
		allowed = True
	if allowed:
		print(line)