import os
import sys
from pathlib import Path


patterns_file_path = sys.argv[1]
with open(patterns_file_path, 'r') as f:
    patterns = list(map(str.strip, f.readlines()))

for path in map(str.strip, sys.stdin):
    if Path(path).name in patterns:
        print(path)