import sys
from pathlib import Path

processed_files = open(sys.argv[1]).readlines()
processed_files = map(str.strip, processed_files)
processed_files = map(lambda p: Path(p).name, processed_files)
# processed_files = map(lambda p: p[:p.rfind('_')] + '.jpg', processed_files)

excess = set(processed_files)

for path in map(str.strip, sys.stdin):
	name = Path(path).name
	if name not in excess:
		print(path)