import sys

from tqdm import tqdm

lines = sys.stdin
lines = tqdm(lines, miniters=50, mininterval=5)
for line in lines:
    print(line, end='')
    