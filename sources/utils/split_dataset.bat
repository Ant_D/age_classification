set input_directory=%1
set output_directory=%2

set test_directory=%output_directory%\test
set val_directory=%output_directory%\val
set train_directory=%output_directory%\train

set test_fraction=0.2
set val_fraction=0.1


dir /s /b %input_directory%\*.jpg | python random_filter.py %test_fraction% | python copy_with_parent.py %test_directory%
dir /s /b %test_directory% > tmp
dir /s /b %input_directory%\*.jpg | python filter_excess.py tmp | python random_filter.py %val_fraction% | python copy_with_parent.py %val_directory%
dir /s /b %val_directory% >> tmp
dir /s /b %input_directory%\*.jpg | python filter_excess.py tmp | python copy_with_parent.py %train_directory%
del tmp