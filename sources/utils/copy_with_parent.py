import os
import shutil
import sys
from pathlib import Path

from tqdm import tqdm

output_directory = sys.argv[1]
os.makedirs(output_directory, exist_ok=True)

paths = map(str.strip, sys.stdin)
paths = tqdm(paths, miniters=50, mininterval=1)
for path in paths:
    parent_name = Path(path).parent.name
    new_parent_path = os.path.join(output_directory, parent_name)
    os.makedirs(new_parent_path, exist_ok=True)
    name = Path(path).name
    new_path = os.path.join(new_parent_path, name)
    shutil.copy(path, new_path)
