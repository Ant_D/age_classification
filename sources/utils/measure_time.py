import os
import sys
import time

start = time.time()
command = ' '.join(sys.argv[1:])
os.system(command)
finish = time.time()
print('Elapsed time:', finish - start, 'sec')