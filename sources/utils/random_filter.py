import random
import sys

from tqdm import tqdm

prob = float(sys.argv[1])

paths = map(str.strip, sys.stdin)
for path in paths:
    if random.random() < prob:
        print(path)
