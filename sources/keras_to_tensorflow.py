"""
    Convert keras model to Tensorflow protobuf.

    Usage:
        python keras_to_tensorflow.py <model path> <protobuf file path>
"""

import sys

import tensorflow as tf


WORK_DIR = '.'
AS_TEXT = False


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Saves model to protobuf.
    Freezes the state of a session into a pruned computation graph.
    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    from tensorflow.python.framework.graph_util import convert_variables_to_constants
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = convert_variables_to_constants(session, input_graph_def,
                                                      output_names, freeze_var_names)
        return frozen_graph


def save_keras_model_as_protobuf(keras_model, protobuf_file_path):
    """Save keras model as tf protobuf file."""
    from tensorflow.keras import backend as K
    frozen_graph = freeze_session(K.get_session(),
                                output_names=[out.op.name for out in keras_model.outputs])
    tf.train.write_graph(frozen_graph, WORK_DIR, protobuf_file_path, as_text=AS_TEXT)


def convert_hdf5_model_to_protobuf_model(hdf5_model_path, protobuf_model_path):
    tf.keras.backend.set_learning_phase(0)
    model = tf.keras.models.load_model(hdf5_model_path, compile=False)
    save_keras_model_as_protobuf(model, protobuf_model_path)


if __name__ == '__main__':
    hdf5_model_path = sys.argv[1]
    protobuf_model_path = sys.argv[2]

    convert_hdf5_model_to_protobuf_model(hdf5_model_path, protobuf_model_path)