import sys

import cv2

from interface import AgeRecognizer
from detect_faces_and_classify import add_visualization

graph_path = sys.argv[1]
age_recognizer = AgeRecognizer(graph_path)

cap = cv2.VideoCapture(0) #cv2.VideoCapture('192.168.0.101:8080/video')

while True:
    ret, frame = cap.read()

    face_boxes, categories = age_recognizer.recognize(frame)

    new_frame = add_visualization(frame, face_boxes, categories)
    
    cv2.imshow('frame', new_frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
