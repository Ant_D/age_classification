import tensorflow as tf

from preprocessing import PreprocessGraphBuilder
from preprocessing import save_preprocessing_graph


IMAGE_SHAPE = (224, 224)
RESIZE_METHOD = tf.image.ResizeMethod.BILINEAR
MEAN = 0
SCALE = 1. / 255
EXPORT_DIR = 'export'


def main():
    builder = PreprocessGraphBuilder(
        image_shape=IMAGE_SHAPE,
        resize_method=RESIZE_METHOD,
        mean=MEAN,
        scale=SCALE,
    )

    save_preprocessing_graph(EXPORT_DIR, builder)


if __name__ == '__main__':
    main()