from .save import zip_folder


def upload_to_googledrive(drive, source, target):
    zip_folder(source)
    file = drive.CreateFile({'title': target + '.zip'})
    file.SetContentFile(source + '.zip')
    file.Upload()
