import numpy as np
import tensorflow as tf
from preprocessing import load_jpeg
from preprocessing import Preprocessor

EXPORT_DIR = 'export'
CHANNELS = 3
IMAGE_PATH = r'../tests/image.jpg'
OUTPUT_IMAGE_SHAPE = (1, 224, 224, 3)


def main():
    preprocessor = Preprocessor.load_from_directory(EXPORT_DIR)

    input_image = load_jpeg(IMAGE_PATH)
    input_image = np.array([input_image])

    output_image = preprocessor.preprocess(input_image)

    assert(output_image.shape == OUTPUT_IMAGE_SHAPE)
    normalized = (0 <= output_image) & (output_image <= 1)
    assert(normalized.all())
    assert(isinstance(output_image[0][0][0][0], np.float32))
    print('OK')


if __name__ == '__main__':
    main()