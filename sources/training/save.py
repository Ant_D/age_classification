import os
import zipfile
from pathlib import Path


def save_plot(plot, plot_name, target_dir):
    plot_path = os.path.join(target_dir, plot_name)
    plot.savefig(plot_path)

    
def save_test_result(result, result_name, target_dir):
    result_path = os.path.join(target_dir, result_name)
    with open(result_path, 'w') as f:
        print(result, file=f)

        
def save_model(model, model_name, target_dir):
    model_path = os.path.join(target_dir, model_name)
    model.save(model_path)

    
def zip_folder(folder_path):
    archive_name = Path(folder_path).name + '.zip'            
    with zipfile.ZipFile(archive_name, 'w', zipfile.ZIP_DEFLATED) as zipobj:
        rootlen = len(folder_path) + 1
        for base, _, files in os.walk(folder_path):
            for file in files:
                fn = os.path.join(base, file)
                zipobj.write(fn, fn[rootlen:])
