import tensorflow as tf


CHANNELS = 3


def load_jpeg_no_session(path):
    jpeg_image = tf.read_file(path)
    image = tf.image.decode_jpeg(jpeg_image, channels=CHANNELS)
    return image 


def load_jpeg(path):
    with tf.Session():
        image = load_jpeg_no_session(path)
        return image.eval()


class Preprocessor:
    INPUT_NAME = 'input'
    OUTPUT_NAME = 'output'
    NAME_SCOPE = 'preprocessor'
    INPUT_TENSOR_NAME = f'{NAME_SCOPE}/{INPUT_NAME}:0'
    OUTPUT_TENSOR_NAME = f'{NAME_SCOPE}/{OUTPUT_NAME}:0'

    @classmethod
    def load_from_directory(cls, directory_path):
        graph = tf.Graph()
        session = tf.Session(graph=graph)
        tf.saved_model.loader.load(
            session, 
            [tf.saved_model.tag_constants.SERVING], 
            directory_path,
            import_scope=cls.NAME_SCOPE
        )
        return cls(session)

    @classmethod
    def create(cls, builder):
        graph = tf.Graph()
        session = tf.Session(graph=graph)
        with session.as_default():
            with tf.name_scope(cls.NAME_SCOPE):
                builder()
                return cls(session)

    def __init__(self, session):
        self.session = session
        self.graph = session.graph
        self.tensor_input = self.session.graph.get_tensor_by_name(self.INPUT_TENSOR_NAME)
        self.tensor_output = self.session.graph.get_tensor_by_name(self.OUTPUT_TENSOR_NAME)

    def preprocess(self, image):
        preprocessed_image = self.session.run(self.tensor_output, feed_dict={self.tensor_input: image})
        return preprocessed_image


class PreprocessGraphBuilder:
    INPUT_SHAPE = (None, None, None, 3)

    def __init__(self, image_shape, resize_method=tf.image.ResizeMethod.BILINEAR, mean=0, scale=1):
        self.image_shape = image_shape
        self.mean = mean
        self.scale = scale

    def preprocess(self, input_image):
        output = tf.image.resize(input_image, self.image_shape)
        output = self.normalize(output)
        return output

    def normalize(self, tensor):
        output = (tensor - self.mean) / (self.scale - self.mean)
        return output

    def __call__(self):
        input_image = tf.placeholder(tf.uint8, shape=self.INPUT_SHAPE, name=Preprocessor.INPUT_NAME)
        image_tensor = self.preprocess(input_image)
        output_image = tf.identity(image_tensor, name=Preprocessor.OUTPUT_NAME)
        return input_image, output_image


def save_preprocessing_graph(path, builder):
    with tf.Session() as session:
        input_image, output_image = builder()
        tf.saved_model.simple_save(
            session, 
            path, 
            inputs={Preprocessor.INPUT_NAME: input_image}, 
            outputs={Preprocessor.OUTPUT_NAME: output_image}
        )
