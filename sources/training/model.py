import tensorflow as tf

IMAGE_SHAPE = (224, 224)


def compile_model(model):
    model.compile(
        optimizer=tf.train.AdamOptimizer(),
        loss=tf.keras.losses.categorical_crossentropy,
        metrics=['accuracy']
    )


def create_model(output_size):
    base_model = tf.keras.applications.MobileNetV2(
        input_shape=IMAGE_SHAPE + (3,),
        include_top=False,
        weights='imagenet'
    )
    base_model.trainable = False

    model = tf.keras.Sequential([
        base_model,
        tf.keras.layers.Dense(64, activation=tf.nn.relu),
        tf.keras.layers.Dropout(rate=0.4),
        tf.keras.layers.Dense(64, activation=tf.nn.relu),
        tf.keras.layers.Dropout(rate=0.4),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(output_size, activation=tf.nn.softmax)
    ])

    compile_model(model)

    return model
