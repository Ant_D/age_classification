import tensorflow as tf


class Trainer:
    def __init__(self, epochs, patience, monitored_metric, checkpoint_period):
        self.epochs = epochs
        self.monitored_metric = monitored_metric
        self.patience = patience
        self.checkpoint_period = checkpoint_period

    def run(self, model, train_dataset, val_dataset, checkpoint_path, verbose=1):
        early_stop_callback = tf.keras.callbacks.EarlyStopping(
            monitor=self.monitored_metric, 
            patience=self.patience,
            verbose=verbose
        )

        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            checkpoint_path, 
            save_weights_only=True,
            verbose=verbose,
            period=self.checkpoint_period
        )
        
        history = model.fit(
            train_dataset.dataset, 
            epochs=self.epochs, 
            steps_per_epoch=train_dataset.min_steps,
            validation_data=val_dataset.dataset,
            validation_steps=val_dataset.min_steps,
            callbacks=[early_stop_callback, checkpoint_callback],
            class_weight=train_dataset.class_weights,
            verbose=verbose
        )

        return history