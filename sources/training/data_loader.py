import os
import pathlib
import re

import numpy as np
import random
import tensorflow as tf

from .preprocessing import load_jpeg_no_session
from .preprocessing import PreprocessGraphBuilder

tf.enable_eager_execution()

AUTOTUNE = tf.data.experimental.AUTOTUNE


class Dataset:
    def __init__(self, x, y, dataset, label_names, min_steps, class_weights):
        self.x = x
        self.y = y
        self.dataset = dataset
        self.label_names = label_names
        self.min_steps = min_steps
        self.class_weights = class_weights


class DataLoader:
    DATA_ROOT_SUBDIRECTORIES = ['train', 'val', 'test']
    USE_FOR_TRAIN = [True, False, False]
    LABEL_NAME_REGEX = re.compile(r'^\D*(\d+)\D+(\d+)$')
    MIN_VALUE_PER_CHANNEL = np.array([0.0, 0.0, 0.0], dtype=np.float32)
    MAX_VALUE_PER_CHANNEL = np.array([255.0, 255.0, 255.0], dtype=np.float32)

    def __init__(self, batch_size, buffer_size, train_aug_and_probs, root_explorer, preprocess_builder):
        self.buffer_size = buffer_size
        self.batch_size = batch_size
        self.train_aug_and_probs = train_aug_and_probs
        self.root_explorer = root_explorer
        self.preprocess_builder = preprocess_builder

    def load(self, data_root):
        for subdir, for_train in zip(self.DATA_ROOT_SUBDIRECTORIES, self.USE_FOR_TRAIN):
            root = pathlib.Path(os.path.join(data_root, subdir))
            attribute = f'{subdir}_dataset_'
            dataset = self._load_dataset(root, for_train)
            self.__setattr__(attribute, dataset)
        self._check_classes_counts()

    def _check_classes_counts(self):
        assert(len(self.train_dataset_.label_names) == len(self.val_dataset_.label_names))
        assert(len(self.train_dataset_.label_names) == len(self.test_dataset_.label_names))

    def _load_dataset(self, root, for_train=True):
        label_names = self.root_explorer.get_label_names(root)
        assert(all(self.LABEL_NAME_REGEX.match(name) for name in label_names))
       
        all_image_paths = self.root_explorer.get_all_file_paths(root)
        random.shuffle(all_image_paths)
        all_image_labels = self._get_all_image_labels(label_names, all_image_paths)
        assert(len(all_image_paths) == len(all_image_labels))
        dataset = self._make_dataset(paths=all_image_paths, labels=all_image_labels, for_train=for_train)

        class_weights = self._calculate_class_weights(label_names, all_image_labels)
        min_steps = self._calculate_min_steps(len(all_image_paths))

        return Dataset(
            x=all_image_paths, 
            y=all_image_labels,
            dataset=dataset, 
            label_names=label_names, 
            min_steps=min_steps, 
            class_weights=class_weights
        )

    def _get_all_image_labels(self, label_names, all_image_paths):
        label_to_index = dict((name, index) for index, name in enumerate(label_names))
        for label, index in label_to_index.items():
            index = self._one_hot(index, len(label_names))
            label_to_index[label] = index

        all_image_labels = [label_to_index[path.parent.name] for path in map(pathlib.Path, all_image_paths)]

        return all_image_labels

    def _one_hot(self, index, total):
        return tf.one_hot(index, total)

    def _make_dataset(self, paths, labels, for_train=False):
        path_ds = tf.data.Dataset.from_tensor_slices(paths)
        image_ds = path_ds.map(load_jpeg_no_session, num_parallel_calls=AUTOTUNE)
        image_ds = image_ds.map(self.preprocess_builder.preprocess, num_parallel_calls=AUTOTUNE)
        
        if for_train:
           for aug, prob in self.train_aug_and_probs:
            image_ds = image_ds.map(
                lambda x: tf.cond(tf.random_uniform([], 0, 1) < prob, lambda: aug(x), lambda: x), 
                num_parallel_calls=AUTOTUNE
            )
        image_ds = self._clip(image_ds)
 
        label_ds = tf.data.Dataset.from_tensor_slices(labels)
        image_label_ds = tf.data.Dataset.zip((image_ds, label_ds))
        
        if for_train:
            image_label_ds = image_label_ds.shuffle(buffer_size=self.buffer_size)
            image_label_ds = image_label_ds.repeat()
        
        image_label_ds = image_label_ds.batch(self.batch_size)
        image_label_ds = image_label_ds.prefetch(buffer_size=AUTOTUNE)

        return image_label_ds

    def _clip(self, image_ds):
        min_value = self.preprocess_builder.normalize(self.MIN_VALUE_PER_CHANNEL)
        min_value = tf.constant(min_value)
        max_value = self.preprocess_builder.normalize(self.MAX_VALUE_PER_CHANNEL)
        max_value = tf.constant(max_value)
        return image_ds.map(lambda x: tf.clip_by_value(x, min_value, max_value))

    def _calculate_min_steps(self, total):
        return (total + self.batch_size - 1) // self.batch_size

    def _calculate_class_weights(self, label_names, all_image_labels):
        regex = self.LABEL_NAME_REGEX
        width = []
        for name in label_names:
            match = regex.match(name)
            max_age = int(match.group(2))
            min_age = int(match.group(1))
            width.append(max_age - min_age + 1)
        width = np.array(width)

        return (len(all_image_labels) / np.sum(all_image_labels, axis=0)) * (width / np.sum(width))

    
class RootExplorer:
    def get_all_file_paths(self, root):
        all_image_paths = root.glob('*/*')
        all_image_paths = map(str, all_image_paths)
        all_image_paths = list(all_image_paths)
        return all_image_paths

    def get_label_names(self, root):
        return sorted(item.name for item in root.glob('*/') if item.is_dir())
