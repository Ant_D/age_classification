import numpy as np
import tensorflow as tf


class Augmentation:
    def __init__(self, image_size):
        self.image_size = image_size
        self.image_shape = (image_size, image_size)


class Flip(Augmentation):
    def __init__(self, image_size):
        super().__init__(image_size)

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
            x = tf.image.random_flip_left_right(x)
            x = tf.image.random_flip_up_down(x)
            return x


class Color(Augmentation):
    def __init__(self, image_size, max_hue_delta, saturation_range, max_brightness_delta, contrast_range):
        super().__init__(image_size)
        self.max_hue_delta = max_hue_delta
        self.saturation_range = saturation_range
        self.max_brightness_delta = max_brightness_delta
        self.contrast_range = contrast_range

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        x = tf.image.random_hue(x, self.max_hue_delta)
        x = tf.image.random_saturation(x, *(self.saturation_range))
        x = tf.image.random_brightness(x, self.max_brightness_delta)
        x = tf.image.random_contrast(x, *(self.contrast_range))
        return x


class Rotate(Augmentation):
    def __init__(self, image_size, max_angle):
        super().__init__(image_size)
        self.max_angle = max_angle

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        angles_shape = [] if len(x.shape) <= 3 else [x.shape[0]]
        angles = tf.random.uniform(angles_shape, 0, self.max_angle, dtype=tf.float32)
        x = tf.contrib.image.rotate(
            x,
            angles,
            interpolation='NEAREST',
            name=None
        )
        return x


class Zoom(Augmentation):
    def __init__(self, image_size, scale_range):
        super().__init__(image_size)
        self.scale_range = scale_range
    
    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        scale = tf.random_uniform([], *(self.scale_range), dtype=tf.float32)
        x1 = y1 = 0.5 - (0.5 * scale)
        x2 = y2 = 0.5 + (0.5 * scale)
        box = [x1, y1, x2, y2]
        
        crops = tf.image.crop_and_resize([x], boxes=[box], box_ind=[0], crop_size=self.image_shape)
        
        return crops[0]


class Grayscale(Augmentation):
    def __init__(self, image_size):
        super().__init__(image_size)

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        x = tf.image.rgb_to_grayscale(x)
        x = tf.image.grayscale_to_rgb(x)
        return x


class Translate(Augmentation):
    def __init__(self, image_size, max_translation):
        super().__init__(image_size)
        self.max_translation = max_translation

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        dx = tf.random_uniform([], 0, 2 * self.max_translation, dtype=tf.int32) - self.max_translation
        dy = tf.random_uniform([], 0, 2 * self.max_translation, dtype=tf.int32) - self.max_translation
        x = tf.contrib.image.translate(x, translations=[dx, dy])
        return x


class GaussianNoise(Augmentation):
    def __init__(self, image_size, max_noise_var):
        super().__init__(image_size)
        self.max_noise_var = max_noise_var

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        noise_var = tf.random_uniform([], 0, self.max_noise_var)
        gaussian = tf.random_normal([self.image_size, self.image_size, 1], 0, noise_var)
        x = x + gaussian
        return x


class SaltPepperNoise(Augmentation):
    def __init__(self, image_size, salt_pepper_factor, salt_factor):
        super().__init__(image_size)
        salt_pepper_amount = salt_pepper_factor * image_size * image_size
        self.salt_amount = int(salt_factor * salt_pepper_amount)
        self.pepper_amount = int((1 - salt_factor) * salt_pepper_amount)

    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        pepper_coords = [np.random.randint(0, self.image_size, self.pepper_amount) for _ in range(2)]
        peppered = np.ones(self.image_shape + (3,))
        peppered[tuple(pepper_coords)] = 0
        x = x * peppered
        
        salt_coords = [np.random.randint(0, self.image_size, self.salt_amount) for _ in range(2)]
        salted = np.ones(self.image_shape + (3,))
        salted[tuple(salt_coords)] = 0
        x = x * salted + (1 - salted)
        
        return x


class Blur(Augmentation):
    def __init__(self, image_size, max_blur_factor):
        super().__init__(image_size)
        self.max_blur_factor = max_blur_factor
    
    def __call__(self, x: tf.Tensor) -> tf.Tensor:
        blur_factor = np.random.uniform(0, self.max_blur_factor)
        shape = ((1 - blur_factor) * np.array(self.image_shape)).astype(int)
        tmp = tf.image.resize_images(
            [x],
            size=shape,
            method=tf.image.ResizeMethod.BICUBIC,
            align_corners=False,
            preserve_aspect_ratio=False
        )[0]
        x = tf.image.resize_images(
            [tmp],
            size=self.image_shape,
            method=tf.image.ResizeMethod.BICUBIC,
            align_corners=False,
            preserve_aspect_ratio=False
        )[0]
        return x