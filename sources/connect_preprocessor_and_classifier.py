import sys

import numpy as np
import tensorflow as tf
from tensorflow.python.framework import meta_graph

from interface import AgeClassifier
from training.preprocessing import Preprocessor


def connect_graphs(first_graph, first_graph_output, second_graph, second_graph_input):
    graph = tf.Graph()

    first_meta_graph = tf.train.export_meta_graph(graph=first_graph)
    meta_graph.import_scoped_meta_graph(
        first_meta_graph,
        graph=graph
    )

    second_meta_graph = tf.train.export_meta_graph(graph=second_graph)
    meta_graph.import_scoped_meta_graph(
        second_meta_graph,
        input_map={second_graph_input.op.name: first_graph_output},
        graph=graph
    )

    return graph


def connect_preprocessor_to_classifier(preprocessor, classifier):
    graph = connect_graphs(
        preprocessor.graph, preprocessor.tensor_output, 
        classifier.graph, classifier.tensor_input
    )
    return graph


def main():
    preprocessor_dir_path = sys.argv[1]
    classifier_graph_path = sys.argv[2]
    output_path = sys.argv[3]

    preprocessor = Preprocessor.load_from_directory(preprocessor_dir_path)
    classifier = AgeClassifier.load_from_protobuf_file(classifier_graph_path)

    graph = connect_preprocessor_to_classifier(preprocessor, classifier)

    tf.io.write_graph(graph, '.', output_path, as_text=False)


if __name__ == '__main__':
    main()