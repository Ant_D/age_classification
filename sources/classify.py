import sys

from interface import AgeClassifier
from training.preprocessing import load_jpeg


def print_scores(classifier):
    output = ''
    for category, score in zip(classifier.CATEGORIES, classifier.scores_):
        output += '{} {}\n'.format(category, score)
    print(output)


if __name__ == '__main__':
    graph_path = sys.argv[1]

    classifier = AgeClassifier.load_from_protobuf_file(graph_path)

    paths = map(str.strip, sys.stdin)
    for image_path in paths:
        image_array = load_jpeg(image_path)
        category = classifier.classify(image_array)
        print(image_path, category)
        print_scores(classifier)