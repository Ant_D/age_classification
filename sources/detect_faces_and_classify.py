import sys

import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

from interface import AgeRecognizer
from training.preprocessing import load_jpeg

def print_result(face_boxes, categories):
    for face_box, category in zip(face_boxes, categories):
        face_box_view = ' '.join(map(str, face_box))
        print(image_path, face_box_view, category)


def add_visualization(image_array, face_boxes, categories):
    FONT_SIZE = 16
    COLOR = (0, 255, 0)
    image = Image.fromarray(image_array)
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype('arial.ttf', FONT_SIZE)
    for face_box, category in zip(face_boxes, categories):
        draw.rectangle(face_box, outline=COLOR)
        draw.text((face_box[0], face_box[1] - FONT_SIZE), category, font=font, fill=COLOR)
    return np.asarray(image)
    

if __name__ == '__main__':
    graph_path = sys.argv[1]

    is_visual = '--visual' in sys.argv[2:]

    age_recognizer = AgeRecognizer(graph_path)

    paths = map(str.strip, sys.stdin)
    for image_path in paths:
        image_array = load_jpeg(image_path)
        face_boxes, categories = age_recognizer.recognize(image_array)
        print_result(face_boxes, categories)
        if is_visual:
            new_image_array = add_visualization(image_array, face_boxes, categories)
            Image.fromarray(new_image_array).show()
