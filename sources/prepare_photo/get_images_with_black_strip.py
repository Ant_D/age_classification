"""
    Filter images with black strip on the border.
"""

import sys

from PIL import Image, ImageFile


def has_black_strip(image, strip_width=5):
    regions = []

    y = min(image.height, strip_width)
    region = image.crop((0, 0, image.width, y))
    regions.append(region)

    y = max(0, image.height - strip_width)
    region = image.crop((0, y, image.width, image.height))
    regions.append(region)

    x = min(image.width, strip_width)
    region = image.crop((0, 0, x, image.height))
    regions.append(region)

    x = max(0, image.width - strip_width)
    region = image.crop((x, 0, image.width, image.height))
    regions.append(region)

    return any(
        all(byte == 0 for byte in region.tobytes())
        for region in regions
    )


if __name__ == '__main__':
    ImageFile.LOAD_TRUNCATED_IMAGES = True

    for image_path in map(str.strip, sys.stdin):
        with Image.open(image_path) as image:
            if has_black_strip(image):
                print(image_path)
