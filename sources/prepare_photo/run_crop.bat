@echo off

set root_path=C:\foto\age

for /l %%i in (7, 1, 9) do (
    echo extracting imdb_%%i
    "C:\Program Files\7-Zip\7z.exe" x -o%root_path%/imdb_%%i %root_path%/imdb_%%i.tar
    echo crop faces from imdb_%%i
    python crop_faces.py %root_path%/imdb_%%i\imdb %root_path%/facedata/path%%i.txt %root_path%/facedata/coord%%i.txt %root_path%/croppedFaces%%i
    echo categorize faces from imdb_%%i
    python categorize.py %root_path%/croppedFaces%%i %root_path%/categories_%%i
)
