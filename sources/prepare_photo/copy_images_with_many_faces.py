import os
import shutil
import sys
from glob import iglob

import dlib
from PIL import Image, ImageFile
from tqdm import tqdm

detector = dlib.get_frontal_face_detector()


def faces_count(image):
    image_array = dlib.load_rgb_image(image_path)
    detections, _, __ = detector.run(image_array, 1, 0)
    return len(detections)


if __name__ == '__main__':
    ImageFile.LOAD_TRUNCATED_IMAGES = True

    source_path = sys.argv[1]
    target_path = sys.argv[2]

    os.makedirs(target_path, exist_ok=True)

    image_count = sum(len(filenames) for _, __, filenames in os.walk(source_path))
    pattern = os.path.join(source_path, '**/*.jpg')
    copied_count = 0

    for image_path in tqdm(iglob(pattern, recursive=True), total=image_count, miniters=50, mininterval=1):
        with Image.open(image_path) as image:
            if faces_count(image) > 1:
                image_name = image_path.split('\\')[-1]
                new_image_path = os.path.join(target_path, image_name)
                shutil.copy(image_path, new_image_path)
                copied_count += 1

    print('Copied', copied_count, 'files')
