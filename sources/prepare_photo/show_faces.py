import sys

import dlib
from PIL import Image, ImageDraw, ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

image_path = sys.argv[1]

detector = dlib.get_frontal_face_detector()
image_array = dlib.load_rgb_image(image_path)
detections, scores, idx = detector.run(image_array, 1, 0.5)

with Image.open(image_path) as image:
    draw = ImageDraw.Draw(image)
    for bbox in detections:
        top_left = bbox.tl_corner()
        bottom_right = bbox.br_corner()
        draw.rectangle([top_left.x, top_left.y, bottom_right.x, bottom_right.y])
    image.show()
