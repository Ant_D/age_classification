import sys

from pascal_voc_writer import Writer
from PIL import Image


def parse_line(line):
    tokens = line.split(' ')
    assert(len(tokens) == 6)
    path = tokens[0]
    box = tokens[1 : 5]
    category = tokens[5]
    return path, box , category
    

def change_extension(path, new_extension):
    path_no_extension, _ = path.rsplit('.', 1)
    return path_no_extension + '.xml'


lines = map(str.strip, sys.stdin)
last_image_path = None
xml_path = None
writer = None
width = 0
height = 0
for line in lines:
    image_path, box, category = parse_line(line)
    if last_image_path != image_path:
        if writer:
            writer.save(xml_path)
        xml_path = change_extension(image_path, 'xml')
        last_image_path = image_path
        with Image.open(image_path) as image:
            width = image.width
            height = image.height
            writer = Writer(image_path, width, height)
    writer.addObject(category, *box)

if writer:
    writer.save(xml_path)
