import sys

lines = map(str.strip, sys.stdin)
for line in lines:
    token = line.split(' ')[0]
    print(token)