@echo off

set dirpath=L:\AgeClassificationDataset
set folders=(croppedFaces4 croppedFaces5)

::call venv\Scripts\activate
for %%f in %folders% do (
	echo %%f
	python categorize.py %dirpath%\%%f %dirpath%\categories	
)