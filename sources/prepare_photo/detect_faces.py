"""
    Detects faces on given images and returns coordinates of their bounding box.

    Usage:
        python detect_faces.py
    Input:
        Image paths, one per line
    Output:
        <Image path> <upper left corner x y> <bottom right corner x y>
"""

import os
import sys
from pathlib import Path

import dlib
import numpy as np
from PIL import Image, ImageDraw, ImageFile
from tqdm import tqdm


DETECTION_THRESHOLD = 0.5
ImageFile.LOAD_TRUNCATED_IMAGES = True


def detect_faces(detector, image_path):
    image_array = dlib.load_rgb_image(image_path)
    detections, _, __ = detector.run(image_array, 1, DETECTION_THRESHOLD)

    face_boxes = []
    for detection in detections:
        top_left = detection.tl_corner()
        bottom_right = detection.br_corner()
        face_box = [
            top_left.x, top_left.y, 
            bottom_right.x, bottom_right.y
        ]
        face_boxes.append(face_box)
    
    return face_boxes


if __name__ == '__main__':
    detector = dlib.get_frontal_face_detector()

    paths = map(str.strip, sys.stdin)
    # paths = tqdm(paths, miniters=50, mininterval=1)
    for image_path in paths:
        face_boxes = detect_faces(detector, image_path)
        for face_box in face_boxes:
            face_box_view = ' '.join(map(str, face_box))
            print(image_path, face_box_view)
