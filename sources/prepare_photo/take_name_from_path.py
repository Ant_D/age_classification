import sys
from pathlib import Path

paths = map(str.strip, sys.stdin)
for path in paths:
    name = Path(path).name
    print(name)