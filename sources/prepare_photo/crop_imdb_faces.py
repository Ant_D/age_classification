"""
    Crop faces from imdb folder.

    Usage:
        python crop_faces.py <imdb folder path> <paths file path> <coords file path> <target path>
"""

import os
import sys

from PIL import Image, ImageFile
from tqdm import tqdm


def read_coords(coords_file_path):
    with open(coords_file_path) as coords_file:
        while True:
            four = [coords_file.readline() for _ in range(4)]
            if not four[-1]:
                break
            coords = list(map(float, four))
            yield coords


def read_paths(paths_file_path):
    with open(paths_file_path) as paths_file:
        for path in paths_file:
            yield path.strip()


def count_file_lines(file_path):
    count = 0
    with open(file_path, 'r') as f:
        for _ in f:
            count += 1
    return count


if __name__ == '__main__':
    ImageFile.LOAD_TRUNCATED_IMAGES = True

    imdb_dirpath = sys.argv[1]
    paths_file_path = sys.argv[2]
    coords_file_path = sys.argv[3]
    target_path = sys.argv[4]

    lines_count = count_file_lines(paths_file_path)
    assert(4 * lines_count == count_file_lines(coords_file_path))

    os.makedirs(target_path, exist_ok=True)
    faces_data = zip(read_paths(paths_file_path), read_coords(coords_file_path))
    for path, coords in tqdm(faces_data, total=lines_count, miniters=50, mininterval=1):
        fullpath = os.path.join(imdb_dirpath, path)
        try:
            image = Image.open(fullpath, 'r')
            cropped_image = image.crop(coords)
        except OSError:
            print('Cant read', fullpath)
        except SystemError:
            print('Incorrect coordinates for', fullpath)
        else:
            dirname, filename = path.split('/')
            dirpath = os.path.join(target_path, dirname)
            if not os.path.isdir(dirpath):
                os.mkdir(dirpath)
            cropped_image_path = os.path.join(dirpath, filename)
            cropped_image.save(cropped_image_path)
