"""
    Crop specified regions from given images and save them in specified directory.

    Usage:
        python crop.py <output directory>
    Input:
        Region descriptions, one per line
        Region description:
            <Image path> <x of upper left corner> <y of upper left corner> <x of bottom right corner> <y of bottom right corner>
"""

import os
import sys
from pathlib import Path

import dlib
import numpy as np
from PIL import Image, ImageFile
from tqdm import tqdm


ImageFile.LOAD_TRUNCATED_IMAGES = True


def parse_line(line):
    tokens = line.split(' ')
    path, *boxes = tokens
    boxes = list(map(int, boxes))
    return path, boxes


if __name__ == '__main__':
    output_directory = sys.argv[1]
    os.makedirs(output_directory, exist_ok=True)

    lines = map(str.strip, sys.stdin)

    image = None
    last_path = None
    count = 0
    for path, box in map(parse_line, lines):
        if path != last_path:
            if last_path:
                image.close()
            image = Image.open(path)
            last_path = path
            count = 0
        count += 1
        region = image.crop(box)
        name = Path(path).name
        name_no_extension, extension = name.rsplit('.', 1)
        region_name = f'{name_no_extension}_{count}.{extension}'
        region_path = os.path.join(output_directory, region_name)
        region.save(region_path)