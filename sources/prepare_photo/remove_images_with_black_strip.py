import os
import shutil
import sys
from glob import iglob

from PIL import Image, ImageFile
from tqdm import tqdm


# def has_black_strip(image, factor=0.05):
#     regions = []

#     y = max(1, int(image.height * factor))
#     region = image.crop((0, 0, image.width, y))
#     regions.append(region)

#     y = max(1, int(image.height * (1 - factor)))
#     region = image.crop((0, y, image.width, image.height))
#     regions.append(region)

#     x = max(1, int(image.width * factor))
#     region = image.crop((0, 0, x, image.height))
#     regions.append(region)

#     x = max(1, int(image.width * (1 - factor)))
#     region = image.crop((x, 0, image.width, image.height))
#     regions.append(region)

#     return any(
#         all(byte == 0 for byte in region.tobytes())
#         for region in regions
#     )


def has_black_strip(image, strip_width=5):
    regions = []

    y = min(image.height, strip_width)
    region = image.crop((0, 0, image.width, y))
    regions.append(region)

    y = max(0, image.height - strip_width)
    region = image.crop((0, y, image.width, image.height))
    regions.append(region)

    x = min(image.width, strip_width)
    region = image.crop((0, 0, x, image.height))
    regions.append(region)

    x = max(0, image.width - strip_width)
    region = image.crop((x, 0, image.width, image.height))
    regions.append(region)

    return any(
        all(byte == 0 for byte in region.tobytes())
        for region in regions
    )


if __name__ == '__main__':
    ImageFile.LOAD_TRUNCATED_IMAGES = True

    source_path = sys.argv[1]
    target_path = sys.argv[2]

    os.makedirs(target_path, exist_ok=True)

    image_count = sum(len(filenames) for _, __, filenames in os.walk(source_path))
    pattern = os.path.join(source_path, '**/*.jpg')
    removed_count = 0

    for image_path in tqdm(iglob(pattern, recursive=True), total=image_count, miniters=50, mininterval=1):
        with Image.open(image_path) as image:
            if has_black_strip(image):
                image_name = image_path.split('\\')[-1]
                new_image_path = os.path.join(target_path, image_name)
                shutil.copy(image_path, new_image_path)
                os.remove(image_path)
                removed_count += 1

    print('Removed', removed_count, 'files')
