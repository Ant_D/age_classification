import sys

from categorize import extract_age


for filepath in map(str.strip, sys.stdin):
    filename = filepath.split('\\')[-1]
    age = extract_age(filename)
    if not age or (age < 0 and age > 1000):
        print(filepath)
