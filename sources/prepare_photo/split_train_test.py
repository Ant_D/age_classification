import os
import random
import shutil
import sys
from pathlib import Path


TEST_FRACTION = 0.1


if __name__ == '__main__':
    target_dir = sys.argv[1]

    train_dir = os.path.join(target_dir, 'train')
    test_dir = os.path.join(target_dir, 'test')
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)

    for path in map(str.strip, sys.stdin):
        directory = test_dir if random.random() < TEST_FRACTION else train_dir
        path = path[:path.rfind('.') + 1]
        new_path = os.path.join(directory, Path(path).name)
        for extension in ['xml', 'jpg']:
            shutil.copy(path + extension, new_path + extension)
