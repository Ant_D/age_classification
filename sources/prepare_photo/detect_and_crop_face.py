"""
    Detect and crop face from given images and save it in specified path.

    Usage:
        python detect_and_crop_face.py <target dir>

    Input:
        image paths, one per line
"""

import os
import sys
from pathlib import Path

import dlib
import numpy as np
from PIL import Image, ImageDraw, ImageFile

PADDING = 0.2
DETECTION_THRESHOLD = -0.2
ImageFile.LOAD_TRUNCATED_IMAGES = True


def crop_face(detector, image_path):
    image_array = dlib.load_rgb_image(image_path)
    detections, _, scores = detector.run(image_array, 1, DETECTION_THRESHOLD)
    if detections:
        index = np.argmax(scores)
        detection = detections[index]
        with Image.open(image_path) as image:
            top_left = detection.tl_corner()
            bottom_right = detection.br_corner()
            xpadding = int(PADDING * (bottom_right.x - top_left.x))
            ypadding = int(PADDING * (bottom_right.y - top_left.y))
            cropped_image = image.crop([
                max(0, top_left.x - xpadding), 
                max(0, top_left.y - ypadding), 
                min(bottom_right.x + xpadding,  image.width - 1),
                min(bottom_right.y + ypadding, image.height - 1)
            ])
            return cropped_image


if __name__ == '__main__':
    target_directory = sys.argv[1]
    os.makedirs(target_directory, exist_ok=True)

    detector = dlib.get_frontal_face_detector()
    for image_path in map(str.strip, sys.stdin):
        cropped_image = crop_face(detector, image_path)
        if cropped_image:
            new_image_path = os.path.join(target_directory, Path(image_path).name)
            cropped_image.save(new_image_path)
        else:
            print('Face not found:', image_path, file=sys.stderr)
