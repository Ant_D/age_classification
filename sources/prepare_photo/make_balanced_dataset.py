"""
    Make new dataset with classes having equal sizes.

    Usage:
        python make_balanced_dataset.py <class_size> <original dataset path> <output dataset path>
"""

import os
import random
import shutil
import sys

from tqdm import tqdm


if __name__ == '__main__':
    category_size = int(sys.argv[1])
    source_path = sys.argv[2]
    target_path = sys.argv[3]

    categories = os.listdir(source_path)

    for category in tqdm(categories):
        source_category_path = os.path.join(source_path, category)

        filenames = os.listdir(source_category_path)
        random.shuffle(filenames)
        filenames = filenames[:category_size]

        target_category_path = os.path.join(target_path, category)
        os.makedirs(target_category_path, exist_ok=True)
        for filename in filenames:
            source_filepath = os.path.join(source_category_path, filename)
            target_filepath = os.path.join(target_category_path, filename)
            shutil.copy(source_filepath, target_filepath)
