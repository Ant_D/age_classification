import os
import shutil
import sys
from glob import iglob
from pathlib import Path

from tqdm import tqdm


class Photo:
    def __init__(self, name, label):
        self.name = name
        self.label = label
    
    def __hash__(self):
        return hash(self.name)
        
    def __eq__(self, other):
        return self.name == other.name


def prepare_name_to_label(dataset_dir):
    filepath_pattern = os.path.join(dataset_dir, '**', '*.jpg')
    filepaths = iglob(filepath_pattern, recursive=True)
    
    name_to_label = dict(
        (Path(path).name, Path(path).parent.name) for path in filepaths
    )

    return name_to_label


if __name__ == '__main__':
    dataset_dir = sys.argv[1]
    target_dir = sys.argv[2]
    
    name_to_label = prepare_name_to_label(dataset_dir)

    for filepath in tqdm(map(str.strip, sys.stdin), miniters=50, mininterval=1):
        filename = Path(filepath).name
        if filename in name_to_label:
            new_filedir = os.path.join(target_dir, name_to_label[filename])
            os.makedirs(new_filedir, exist_ok=True)
            new_filepath = os.path.join(new_filedir, filename)
            shutil.copy(filepath, new_filepath)
