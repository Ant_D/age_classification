"""
    Extract faces from given images and save them in specified directory.

    Usage:
        python extract_faces.py <output directory>
    Input:
        Image paths, one per line
"""

import os
import sys
from pathlib import Path

import dlib
import numpy as np
from PIL import Image, ImageDraw, ImageFile
from tqdm import tqdm


PADDING = 0.1
DETECTION_THRESHOLD = 0.7
ImageFile.LOAD_TRUNCATED_IMAGES = True


def extract_faces(detector, image_path):
    image_array = dlib.load_rgb_image(image_path)
    detections, _, __ = detector.run(image_array, 1, DETECTION_THRESHOLD)

    faces = []
    with Image.open(image_path) as image:
        for detection in detections:
            top_left = detection.tl_corner()
            bottom_right = detection.br_corner()
            xpadding = int(PADDING * (bottom_right.x - top_left.x))
            ypadding = int(PADDING * (bottom_right.y - top_left.y))
            face = image.crop([
                max(0, top_left.x - xpadding), 
                max(0, top_left.y - ypadding), 
                min(bottom_right.x + xpadding,  image.width - 1),
                min(bottom_right.y + ypadding, image.height - 1)
            ])
            faces.append(face)
    
    return faces


if __name__ == '__main__':
    output_directory = sys.argv[1]
    os.makedirs(output_directory, exist_ok=True)

    detector = dlib.get_frontal_face_detector()
    for image_path in tqdm(map(str.strip, sys.stdin), miniters=50, mininterval=1):
        faces = extract_faces(detector, image_path)
        name_no_extension, extension = Path(image_path).name.rsplit('.', 1)
        if faces:
            for index, face in enumerate(faces):
                face_id = ''.join([name_no_extension, f'_{index}', '.', extension])
                face_path = os.path.join(output_directory, face_id)
                face.save(face_path)
        #else:
        #    print('faces not found', image_path)
