"""
    Fit images into given restrictions of width and height.

    Usage:
        python fit_size.py <max_width> <max_height> <output_directory>

    Input:
        Image paths, one per line
"""

import os
import sys
from glob import iglob
from pathlib import Path

from PIL import Image
from PIL import ImageFile
from tqdm import tqdm


def fit(image, max_width, max_height):
    factor = 1
    if max_width < image.width:
        factor = min(factor, max_width / image.width)
    if max_height < image.height:
        factor = min(factor, max_height / image.height)
    if factor < 1:
        new_width = int(factor * image.width)
        new_height = int(factor * image.height)
        image = image.resize((new_width, new_height), resample=Image.BICUBIC)
    return image


if __name__ == '__main__':
    ImageFile.LOAD_TRUNCATED_IMAGES = True

    max_width = int(sys.argv[1])
    max_height = int(sys.argv[2])
    output_directory = sys.argv[3]

    os.makedirs(output_directory, exist_ok=True)

    image_paths = map(str.strip, sys.stdin)

    for image_path in tqdm(image_paths, miniters=50, mininterval=1):
        with Image.open(image_path) as image:
            resized_image = fit(image, max_width, max_height)
            image_name = Path(image_path).name
            resized_image_path = os.path.join(output_directory, image_name)
            resized_image.save(resized_image_path)
