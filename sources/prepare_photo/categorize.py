"""
    Categorize cropped faces photo by age.
    
    Photo name example: nm0001401_rm4158036736_1975-6-4_2004.jpg
    Here 1975 is birth year and 2004 is year of taking photo.
    
    Usage:
        python categorize.py <source path> <target path>
        
    Script takes photos from file tree rooted at <source path> and
    use their copies for creating categories in <target path>.
"""

import os
import re
import shutil
import sys
from glob import glob
from pathlib import Path

from tqdm import tqdm

age_categories = [
    [0, 2],
    [3, 7],
    [8, 12],
    [13, 18],
    [19, 26],
    [27, 35],
    [36, 48],
    [49, 60],
    [61, 75],
    [76, 1000],
]

pattern = re.compile(r'^nm\d+_rm\d+_(?P<birth_year>\d{4})-\d{1,2}-\d{1,2}_(?P<photo_year>\d{4})\.jpg$')


def extract_age(filename):
    match = pattern.match(filename)
    if match:
        birth_year = int(match.group('birth_year'))
        photo_year = int(match.group('photo_year'))
        return photo_year - birth_year


def find_category(age):
    for category in age_categories:
        if category[0] <= age <= category[1]:
            return category


def category_folder_name(category):
    return 'from_{}_to_{}'.format(*category)


def categorize(source_path, target_path):
    os.makedirs(target_path, exist_ok=True)
    
    filepath_pattern = os.path.join(source_path, '**', '*.jpg')
    filepaths = glob(filepath_pattern, recursive=True)

    for filepath in tqdm(filepaths, mininterval=1, miniters=50):
        filename = Path(filepath).name
        age = extract_age(filename)

        category_name = ''
        if age is not None:
            age_category = find_category(age)
            if age_category:
                category_name = category_folder_name(age_category)

        if category_name:
            category_path = os.path.join(target_path, category_name)
            if not os.path.isdir(category_path):
                os.mkdir(category_path)
            new_filepath = os.path.join(category_path, filename)
            shutil.copy(filepath, new_filepath)


if __name__ == '__main__':
    source_path = sys.argv[1]
    target_path = sys.argv[2]
    categorize(source_path, target_path)
