import sys
from pathlib import Path


for image_path in map(str.strip, sys.stdin):
    xml_path = image_path[:image_path.rfind('.') + 1] + "xml"
    print(image_path)
    print(xml_path)