import os
import shutil
import sys

import dlib
import pandas as pd
from PIL import Image, ImageDraw, ImageFile
from tqdm import tqdm


PADDING = 0.1
DETECTION_THRESHOLD = -0.2
ImageFile.LOAD_TRUNCATED_IMAGES = True


def make_image_name(row):
    image_name = 'coarse_tilt_aligned_face.{}.{}'.format(row['face_id'], row['original_image'])
    return image_name


def crop_face(detector, image_path):
    try:
        image_array = dlib.load_rgb_image(image_path)
    except Exception as e:
        print('Cant open', image_path)
        return
    detections, _, __ = detector.run(image_array, 1, DETECTION_THRESHOLD)
    if detections:
        detection = detections[0]
        with Image.open(image_path) as image:
            top_left = detection.tl_corner()
            bottom_right = detection.br_corner()
            xpadding = int(PADDING * (bottom_right.x - top_left.x))
            ypadding = int(PADDING * (bottom_right.y - top_left.y))
            cropped_image = image.crop([
                max(0, top_left.x - xpadding), 
                max(0, top_left.y - ypadding), 
                min(bottom_right.x + xpadding,  image.width - 1),
                min(bottom_right.y + ypadding, image.height - 1)
            ])
            return cropped_image


if __name__ == '__main__':
    dataset_root = sys.argv[1]
    data_path = sys.argv[2]
    target_dir = sys.argv[3]

    data = pd.read_csv(data_path, sep='\t')

    detector = dlib.get_frontal_face_detector()

    for i in tqdm(range(len(data))):
        row = data.loc[i]
        image_name = make_image_name(row)
        image_path = os.path.join(dataset_root, row['user_id'], image_name)
        cropped_image = crop_face(detector, image_path)
        if cropped_image:
            category = row['age']
            category_path = os.path.join(target_dir, category)
            os.makedirs(category_path, exist_ok=True)
            new_image_path = os.path.join(category_path, image_name)
            cropped_image.save(new_image_path)
    
