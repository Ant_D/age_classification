import sys
from pathlib import Path

from categorize import extract_age

for image_path in map(Path, sys.stdin):
    image_name = image_path.name
    age = extract_age(image_name)
    if age == 0:
        print(image_path)
