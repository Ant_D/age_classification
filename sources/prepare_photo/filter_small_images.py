import sys

from PIL import Image


MAX_LENGTH = 500


def issmall(image):
    return image.width < MAX_LENGTH and image.height < MAX_LENGTH


for image_path in map(str.strip, sys.stdin):
    with Image.open(image_path) as image:
        if issmall(image):
            print(image_path)

