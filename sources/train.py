import math
import os
import sys
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf

from connect_preprocessor_and_classifier import connect_preprocessor_to_classifier
from interface.age_classifier import AgeClassifier
from keras_to_tensorflow import convert_hdf5_model_to_protobuf_model
from training.augmentation import *
from training.data_loader import RootExplorer
from training.data_loader import DataLoader
from training.model import create_model
from training.trainer import Trainer
from training.preprocessing import Preprocessor
from training.preprocessing import PreprocessGraphBuilder
from training.preprocessing import save_preprocessing_graph
from training.save import save_model
from training.save import save_plot
from training.save import save_test_result


CHECKPOINT_NAME = 'cp-{epoch:04d}-{val_acc}.ckpt'


def make_history_plots(history, title_prefix, verbose=1):
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(1, len(acc) + 1)

    plt.plot(epochs, loss, 'b--', label='Потери обучения')
    plt.plot(epochs, val_loss, 'r', label='Потери проверки')
    plt.title(title_prefix + '\nПотери во время обучения и проверки')
    plt.xlabel('Эпохи')
    plt.ylabel('Потери')
    plt.legend()
    loss_figure = plt.gcf()
    if verbose:
        plt.show()

    plt.plot(epochs, acc, 'b--', label='Точность обучения')
    plt.plot(epochs, val_acc, 'r', label='Точность проверки')
    plt.title(title_prefix + '\nТочность во время обучения и проверки')
    plt.xlabel('Эпохи')
    plt.ylabel('Точность')
    plt.legend()
    acc_figure = plt.gcf()
    if verbose:
        plt.show()
    
    return loss_figure, acc_figure


def test(model, test_dataset):
    return model.evaluate(test_dataset.dataset, steps=test_dataset.min_steps)


def make_confusion_matrix(model, dataset):
    predictions = model.predict(dataset.dataset, steps=dataset.min_steps)
    predictions = np.argmax(predictions, axis=1)
    labels = np.array(dataset.y)
    total_per_class = np.sum(labels, axis=0)
    labels = np.argmax(labels, axis=1)
    cm = tf.math.confusion_matrix(labels, predictions).numpy()
    cm = (cm.T / total_per_class).T
    return pd.DataFrame(cm, index=dataset.label_names, columns=dataset.label_names)


def train(data_loader, trainer, model, output_directory, verbose=1):
    checkpoint_path = os.path.join(output_directory, CHECKPOINT_NAME)

    history = trainer.run(
        model=model, 
        train_dataset=data_loader.train_dataset_, 
        val_dataset=data_loader.val_dataset_, 
        checkpoint_path=checkpoint_path, 
        verbose=verbose,
    )

    hdf5_model_path = os.path.join(output_directory, 'model.h5')
    model.save(hdf5_model_path)

    preprocessor_path = os.path.join(output_directory, 'preprocessor')
    save_preprocessing_graph(preprocessor_path, data_loader.preprocess_builder)

    # TODO: Make code below to work
    # protobuf_model_path = os.path.join(output_directory, 'model.pb')
    # convert_hdf5_model_to_protobuf_model(hdf5_model_path, protobuf_model_path)
    # preprocessor = Preprocessor.load_from_directory(preprocessor_path)
    # classifier = AgeClassifier.load_from_protobuf_file(protobuf_model_path)
    # inference_model_graph = connect_preprocessor_to_classifier(preprocessor, classifier)
    # inference_model_path = os.path.join(output_directory, 'inference_model.pb')
    # tf.io.write_graph(inference_model_graph, '.', inference_model_path, as_text=False)

    plots = make_history_plots(
        history=history, 
        title_prefix=output_directory, 
        verbose=verbose
    )
    for index, plot in enumerate(plots):
        plot_name = 'plot_{}'.format(index)
        save_plot(plot, plot_name, output_directory)

    test_result = test(model, data_loader.test_dataset_)
    save_test_result(test_result, 'test_result.txt', output_directory)

    cm = make_confusion_matrix(
        model=model, 
        dataset=data_loader.test_dataset_, 
    )
    cm.to_excel(os.path.join(output_directory, 'confusion_matrix.xls'))


def main():
    IMAGE_SIZE = 224
    IMAGE_SHAPE = (IMAGE_SIZE, IMAGE_SIZE)
    MAX_BYTE_VALUE = 255
    BATCH_SIZE = 32
    BUFFER_SIZE = 500

    AUGMENTATIONS_AND_PROBS = [
        (
            Flip(IMAGE_SIZE), 
            0.25
        ),
        (
            Color(
                IMAGE_SIZE, 
                max_hue_delta=0.02, 
                saturation_range=(0.6, 1.4), 
                max_brightness_delta=0.1,
                contrast_range=(0.4, 1.6)
            ), 
            0.25
        ),
        (
            Zoom(
                IMAGE_SIZE,
                scale_range=(0.7, 1.0)
            ),
            0.25
        ),
        (
            Rotate(
                IMAGE_SIZE,
                max_angle=math.pi,
            ),
            0.25
        ),
        (
            Grayscale(IMAGE_SIZE),
            0.05
        ),
        (
            Translate(
                IMAGE_SIZE, 
                max_translation=IMAGE_SIZE // 4
            ),
            0.25
        ),
        (
            GaussianNoise(
                IMAGE_SIZE,
                max_noise_var=0.07
            ),
            0.25
        ),
        (
            SaltPepperNoise(
                IMAGE_SIZE,
                salt_pepper_factor=0.005,
                salt_factor=0.5
            ),
            0.25
        ),
        (
            Blur(
                IMAGE_SIZE,
                max_blur_factor=0.8
            ), 
            0.25
        )
    ]

    EPOCHS = 100
    PATIENCE = 1
    CHECKPOINT_PERIOD = 20
    MONITORED_METRIC = 'val_loss'

    data_root = Path(sys.argv[1])

    preprocessor = PreprocessGraphBuilder(
        image_shape=IMAGE_SHAPE,
        mean=np.array([123.68, 116.779, 103.939], dtype=np.float32),
        scale=MAX_BYTE_VALUE
    )

    data_loader = DataLoader(
        batch_size=BATCH_SIZE,
        buffer_size=BUFFER_SIZE,
        train_aug_and_probs=AUGMENTATIONS_AND_PROBS,
        root_explorer=RootExplorer(),
        preprocess_builder=preprocessor,
    )
    data_loader.load(data_root)

    trainer = Trainer(
        epochs=EPOCHS,
        patience=PATIENCE, 
        monitored_metric=MONITORED_METRIC,
        checkpoint_period=CHECKPOINT_PERIOD
    )

    output_size = len(data_loader.train_dataset_.label_names)
    model = create_model(output_size)

    now = str(datetime.now().replace(microsecond=0)).replace(':', '-')
    output_directory = 'result ' + now

    os.makedirs(output_directory)
    train(data_loader=data_loader, trainer=trainer, model=model, output_directory=output_directory)


if __name__ == '__main__':
    main()