from .age_classifier import AgeClassifier
from .face_detector import FaceDetector


def crop(image_array, box):
    return image_array[
        box[1] : box[3], 
        box[0] : box[2]
    ]


class AgeRecognizer:
    def __init__(self, graph_path):
        self.detector = FaceDetector()
        self.classifier = AgeClassifier.load_from_protobuf_file(graph_path)
       
    def recognize(self, image_array):
        face_boxes = self.detector.detect_faces(image_array)
        categories = []
        for face_box in face_boxes:
            cropped_face = crop(image_array, face_box)
            category = self.classifier.classify(cropped_face)
            categories.append(category)
        return face_boxes, categories
