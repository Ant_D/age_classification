import dlib
import numpy as np


class FaceDetector:
    DETECTION_THRESHOLD = 0.5

    def __init__(self):
        self.detector = dlib.get_frontal_face_detector()

    def detect_faces(self, image_array):
        detections, _, __ = self.detector.run(image_array, 1, self.DETECTION_THRESHOLD)

        image_width = image_array.shape[1]
        image_height = image_array.shape[0]

        face_boxes = []
        for detection in detections:
            top_left = detection.tl_corner()
            bottom_right = detection.br_corner()
            face_box = [
                max(0, top_left.x), 
                max(0, top_left.y), 
                min(image_width, bottom_right.x), 
                min(image_height, bottom_right.y)
            ]
            face_boxes.append(face_box)

        return face_boxes