import dlib
import numpy as np
import tensorflow as tf
from tensorflow.python.platform import gfile


def get_input_name(sess):
    for op in sess.graph.get_operations():
        if 'input' in op.name:
            return op.name


def get_output_name(sess):
    for op in sess.graph.get_operations():
        if 'Softmax' in op.name:
            return op.name


def load_graph_to_session(graph_path, session, name_scope=None):
    with gfile.GFile(graph_path, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        with session.graph.as_default():
            tf.import_graph_def(graph_def, name=name_scope)


class AgeClassifier:
    CATEGORIES = ['00-02', '03-07', '08-12', '13-18', '19-26', '27-35', '36-48', '49-60', '61-75', '76-90']
    NAME_SCOPE = 'classifier'

    @classmethod
    def load_from_protobuf_file(cls, graph_path):
        graph = tf.Graph()
        session = tf.Session(graph=graph)
        load_graph_to_session(graph_path, session, name_scope=cls.NAME_SCOPE)
        return cls(session)

    def __init__(self, session):
        self.graph = session.graph
        self.session = session
        self.input_name = get_input_name(self.session)
        self.output_name = get_output_name(self.session)
        self.tensor_input = self.session.graph.get_tensor_by_name(self.input_name + ':0')
        self.tensor_output = self.session.graph.get_tensor_by_name(self.output_name + ':0')

    def choose_category(self, scores):
        indexes = np.arange(len(scores))
        mean_index = np.sum(indexes * scores)
        category_index = int(round(mean_index))
        return self.CATEGORIES[category_index]

    def classify(self, image_array):
        image_tensor = np.array([image_array])
        self.scores_ = self.session.run(self.tensor_output, {self.tensor_input: image_tensor})[0]
        category = self.choose_category(self.scores_)
        return category